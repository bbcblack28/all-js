const num = document.querySelectorAll('.num');
num.forEach((counter)=>{
    // console.log(counter);
    counter.innerHTML = '0';
    const  updateCounter = ()=>{
        const targetCount = +counter.getAttribute('data-target');
        // console.log(targetCount);
        const startnum = +counter.innerHTML;
        
        const incr = targetCount / 10 ;
        if(startnum < targetCount){
            counter.innerHTML = `${Math.round(startnum + incr) }`
            setTimeout(updateCounter,500)
        }else{
            counter.innerHTML = targetCount;
        }

    };
    updateCounter();
});
